import random, re, os, pickle, subprocess, datetime, difflib, webbrowser
from tkinter import *
from tkinter.filedialog import asksaveasfilename, askdirectory, askopenfilename
from tkinter.messagebox import showerror

def codepoints(s):
    'Posted by Peter Otten on comp.lang.python'
    s = iter(s)
    for c in s:
        if 0xd800 <= ord(c) <= 0xdfff:
            c += next(s, "")
        yield ord(c)

class CodeFrame(LabelFrame):
    def __init__(self, master, val=1, txt = 'Codes'):
        LabelFrame.__init__(self, master, text=txt)
        codevar = self.codevar = IntVar()
        codevar.set(val)
        Radiobutton(self, value = 1, text= '1 Byte', variable = codevar).grid(sticky=W)
        Radiobutton(self, value = 2, text= '2 Bytes', variable = codevar).grid(sticky=W)
        Radiobutton(self, value = 3, text= '3 Bytes', variable = codevar).grid(sticky=W)
        Radiobutton(self, value = 4, text= '4 Bytes', variable = codevar).grid(sticky=W)
        Radiobutton(self, value = 5, text= 'All', variable = codevar).grid(sticky=W)
        for r in range(5):
            self.rowconfigure(r,weight=1)
            
    def get(self):
        return self.codevar.get()
    
class ErrorsFrame(LabelFrame):
    def __init__(self, master, errors='none', txt = 'Errors'):
        LabelFrame.__init__(self, master, text=txt)
        modevar = self.modevar = StringVar()
        modevar.set(errors)
        Radiobutton(self, value = 'none', text= 'None', variable = modevar).grid(sticky=W)
        Radiobutton(self, value = 'ignore', text= 'Ignore', variable = modevar).grid(sticky=W)
        Radiobutton(self, value = 'replace', text= 'Replace', variable = modevar).grid(sticky=W)
        for r in range(3):
            self.rowconfigure(r,weight=1)
  
    def get(self):
        return self.modevar.get()
        
class FileFrame(LabelFrame):
    def __init__(self, master, txt, base):
        LabelFrame.__init__(self, master, text=txt)
        self.namevar = StringVar()
        self.namevar.set(base)
        self.entry = Entry(self, textvariable=self.namevar)
        button = Button(self, text='Browse...', command=self.setDlg)
        self.entry.grid(row=0, column = 0, sticky = 'ew')
        button.grid(row=0, column=1)   
        
    def get(self):
        return self.namevar.get()
    
    def set(self, name):
        self.namevar.set(name)
        
    def setDlg(self):
        s = asksaveasfilename(title="File Base Name", 
                    filetypes=[("Decoded Files", '.txt'),
                               ("Encoded Files", '.bin'), 
                               ("All Files", '.*')])
        if s:
            fname = os.path.basename(s)
            base = os.path.splitext(fname)[0]
            self.namevar.set(base)
            
class DirectoryFrame(LabelFrame):
    def __init__(self, master, title, dataDir):
        LabelFrame.__init__(self, master, text=title)
        self.namevar = StringVar()
        self.namevar.set(dataDir)
        self.entry = Entry(self, textvariable=self.namevar)
        self.entry.xview_moveto(1)
        button = Button(self, text='Browse...', command=self.set)
        self.entry.grid(row=0, column = 0, sticky = 'ew')
        button.grid(row=0, column=1)
        
    def get(self):
        return self.namevar.get()
    
    def set(self):
        s = askdirectory(initialdir=self.get())
        if s:            
            self.namevar.set(os.path.normpath(s))
            self.entry.xview_moveto(1)
            
class MarsFrame(LabelFrame):
    def __init__(self, master, txt, mars):
        LabelFrame.__init__(self, master, text=txt)
        self.namevar = StringVar()
        self.namevar.set(mars)
        self.entry = Entry(self, textvariable=self.namevar)
        self.entry.xview_moveto(1.0)
        button = Button(self, text='Browse...', command=self.setDlg)
        self.entry.grid(row=0, column = 0, sticky = 'ew')
        button.grid(row=0, column=1)        
    def get(self):
        return self.namevar.get()
    def set(self, name):
        self.namevar.set(name)
    def setDlg(self):
        s = askopenfilename(title="MARS Jar File", 
                    filetypes=[("Jar Files", '.jar')])
        if s:
            self.namevar.set(os.path.normpath(s))
            self.entry.xview_moveto(1.0)
            
class ProgFrame(LabelFrame):
    def __init__(self, master, txt, asm):
        LabelFrame.__init__(self, master, text=txt)
        self.namevar = StringVar()
        self.namevar.set(asm)
        self.entry = Entry(self, textvariable=self.namevar)
        self.entry.xview_moveto(1.0)
        button = Button(self, text='Browse...', command=self.setDlg)
        self.entry.grid(row=0, column = 0, sticky = 'ew')
        button.grid(row=0, column=1)        
    def get(self):
        return self.namevar.get()
    def set(self, name):
        self.namevar.set(name)
    def setDlg(self):
        direct = os.path.dirname(self.get())
        s = askopenfilename(title="Main Program File", 
                    filetypes=[("MIPS Files", '.s'),("MIPS Files", '.asm')],
                    initialdir = direct)
        if s:
            self.namevar.set(os.path.normpath(s))
            self.entry.xview_moveto(1.0)

class SampleFrame(LabelFrame):
    def __init__(self, master, title, size):
        LabelFrame.__init__(self, master, text=title)
        self.sizevar = StringVar()
        self.sizevar.set(size)
        entry = Entry(self, justify=RIGHT, textvariable=self.sizevar)
        entry.grid(sticky='ew')
    def get(self):
        return int(self.sizevar.get())
    
class ScrolledText(Frame):
    #Adapted from "Programming Python"
    
    def __init__(self, parent=None, text='', file=None):
        Frame.__init__(self, parent)
        self.makeWidgets()
        self.setText(text, file)
        self.text.configure(state=DISABLED)
        
    def makeWidgets(self):
        
        sbar = Scrollbar(self)
        text = Text(self, relief=SUNKEN, wrap=WORD, width=65, height = 6)
        sbar.config(command=text.yview)                  # xlink sbar and text
        text.config(yscrollcommand=sbar.set)             # move one moves other
        sbar.pack(side=RIGHT, fill=Y)                    # pack first=clip last
        text.pack(side=LEFT, expand=YES, fill=BOTH)      # text clipped first
        self.text = text

    def setText(self, text='', file=None):
        self.insertText('1.0', text, file)
        
    def addText(self, text='', file=None):
        self.insertText(END, text, file)
        self.text.yview_moveto(1.0)
        
    def insertText(self, where, text='', file=None):
        self.text.configure(state=NORMAL)
        if file:
            text = file.read()
        self.text.insert(where, text)                      # add at end
        self.text.configure(state=DISABLED)

    def getText(self):                                   # returns a string
        return self.text.get('1.0', END+'-1c')           # first through last
        
    
class DecoderApp(Frame):
    def __init__(self, master, parms, *args, **kwargs):
        Frame.__init__(self, master, *args, **kwargs)
        self.grid()
        self.makeWidgets(parms)
        
    def makeWidgets(self, parms):
        try:
            codelen = parms['codelen']
        except KeyError:
            codelen = 1
        codes = self.codes = CodeFrame(self, val=codelen)
        codes.grid(row = 0, column=0, rowspan = 5, sticky = 'ns')
        try:
            errs = parms['errs']
        except KeyError:
            errs = 'none'
        errors = self.errors = ErrorsFrame(self, errs)
        errors.grid(row = 0, column=1, rowspan = 5, sticky='ns', padx=2)
        try:
            size =parms['sampleSize']
        except KeyError:
            size =100
        sample = self.sample = SampleFrame(self, 'Sample Size', size=size)
        sample.grid(row=0, column=2, sticky = 'ew',padx=2)
        try:
            dd = parms['dataDir']
        except KeyError:
            dd = os.getcwd()
        dFrame = self.dir = DirectoryFrame(self, 'Data Directory', dd)
        dFrame.grid(row=1, column=2, sticky='ew', padx=2)
        try:
            mars = parms['jar']
        except KeyError:
            mars = ''
        mFrame = self.mars = MarsFrame(self, 'MARS Jar File', mars)
        mFrame.grid(row=2, column=2, sticky='ew',padx=2)
        try:
            prog = parms['program']
        except KeyError:
            prog = ''
        pFrame = self.prog = ProgFrame(self, 'MIPS Program File', prog)
        pFrame.grid(row=3, column=2, sticky='ew',padx=2)
        try:
            base = parms['basename']
        except KeyError:
            base = ''
        bFrame = self.fileBase = FileFrame(self, 'File Base Name', base)
        bFrame.grid(row=4, column=2, sticky='ew',padx=2)
        
        text = self.text = ScrolledText(self)
        text.grid(row=0, column=3, rowspan=5, sticky = 'ns', padx=2)
        
        buttonBar = Frame(self, relief=GROOVE, bd=2)
        buttonBar.grid(row=5, column = 0, columnspan=4, pady=4, sticky = 'ew')
        for c in range(3):
            buttonBar.columnconfigure(c, weight=1)
        
        start = Button(buttonBar, text='New Test', command=self.newTest, width = 9)
        start.grid(row=0, column = 0, padx=5 )
        rerun = Button(buttonBar, text='Re-Run', command=self.test, width = 9)
        rerun.grid(row=0, column = 1, padx = 5)
        helpb = Button(buttonBar, text='Help', command=self.help, width = 9)
        helpb.grid(row=0, column = 2, padx = 5)
        
    def newTest(self):
        codelen = self.codes.get()
        sampleSize = self.sample.get()
        directory = self.dir.get()
        direct = self.dir.get()
        base = self.fileBase.get()
        if not base:
            showerror('Missing Data', 'File Base Name Not Specified') 
            return   
        errors = self.errors.get()
        infile = os.path.join(direct, base+'.bin')
        if codelen == 1:
            pop = range(0, 0x80)
        elif codelen == 2:
            pop = range(0x80, 0x800)
        elif codelen == 3:
            pop = range(0x800, 0x10000)
        elif codelen == 4:
            pop = range(0x10000, 0x110000)
        elif codelen == 5:
            pop = range(0, 0x110000)
                
        if sampleSize > len(pop):
            sample = pop
        else:
            sample = random.sample(pop, sampleSize)
        sample = [s for s in sample if not 0xD800 <= s <= 0xDFFF]
        with open(infile, 'wb') as fin:
            for s in sample:
                x = random.random()       # 0.0 <= x < 1.0
                if errors == 'none':
                    b = chr(s).encode()
                else:
                    b = self.fiddle(x, s)
                fin.write(b)
        self.test()
        
    def test(self):
        fmt = '%a %d %B %Y %I:%M:%S %p'      # timestamp format        
        direct = self.dir.get()
        base = self.fileBase.get()
        infile  = os.path.join(direct, base + '.bin')
        testfile = os.path.join(direct, base + '.txt')
        outfile = os.path.join(direct, base + '_mips.txt')
        htmlfile = os.path.join(direct, base + '.htm')
        jar = self.mars.get()
        prog = self.prog.get()
        errors = self.errors.get()
        
        if not os.path.exists(infile):
            showerror('Missing Input File', os.path.normpath(infile))
            return
        
        self.decode(infile, testfile, errors)
        
        # Run MIPS progam
        errCode = {'none':'n', 'ignore':'i', 'replace':'r'}[errors]
        cmd = ['java', '-jar', jar, 'p', 'sm', 'nc', prog, 'pa', errCode, infile, outfile]
        proc = subprocess.Popen(cmd, stdout=subprocess.PIPE, universal_newlines=True)
        proc.wait()
        result = proc.stdout.read()
        self.text.addText(text = result)

        #copyRight = 'MARS 4.1  Copyright 2003-2011 Pete Sanderson and Kenneth Vollmar\n\n\n'
        #if result == copyRight:
        if result == '\n':
            computed = open(outfile).read()
            expected = open(testfile).read()
            
            if computed == expected:
                self.text.addText(text = '%s passed: Computed output is as expected\n' %base)
                
                
            else:
                self.text.addText(text = '%s failed: See %s\n' %(base, os.path.split(htmlfile)[-1]))
                # Compare to expected output
                
                with open(htmlfile, 'w') as fout:
                    fout.write(difflib.HtmlDiff().make_file(expected,computed,
                                fromdesc=base+'.txt', todesc=base+'_mips.txt'))
                webbrowser.open_new_tab(htmlfile)
            
        timestamp = datetime.datetime.now().strftime(fmt)
        self.text.addText(text=timestamp + '\n\n')
        
    def fiddle(self, p, s):
    # Encode the character chr(s) in UTF-8.  Depending on the value of p, introduce an
    # error into the code (or not).  Return the resulting code.
    
        b = bytearray(chr(s).encode())
        if p <= .08:       # substitute an initial byte from another range 
            if s < 0x80:
                b[0] = random.choice(range(0xC2, 0xF4)) 
            elif s < 0x800:
                b[0] = random.choice(range(0xE0, 0xF4))
            elif s < 0x10000:
                b[0] = random.choice(range(0xF0, 0xF4))
            else:
                b[0] = random.choice(range(0xC2, 0xF0))        
        elif p <= .16:     # replace initial byte by continuation byte
            b[0] = random.choice(range(0x80, 0xC0))
        elif p <= .24:     # insert extra continuation byte
            b.insert(1, random.choice(range(0x80, 0xC0)))
        elif p <= .32:     # drop a continuation byte (if multi-byte code)
            if s >= 0x80:
                b[0:2] = [b[0]]
        elif p <= .40:     # replace continuation byte by initial byte (multi-byte only)
            if s >= 0x80:
                b[1] = random.choice(range(0xC2, 0xF4))
        elif p <= .48:     # replace by surrogate code point
            b[0] = 0xED
            b[1:] = [random.choice((0x80, 0xA0)), random.choice((0x80, 0xC0))]
        elif p <= .56:     # replace initial byte by F4  
            b[0] = 0xF4
        elif p <= .64:     # initial byte in (C0, C1, F5, F6, ...FF)
            b[0] = random.choice([0xC0, 0xC2]+list(range(0xF5, 256)))
        elif p <= .72:     # replace initial byte by E0 and secobd byte 
            if s >= 0x80:  # by incompatible value, if multi-byte
                b[0] = 0xE0
                b[1] = random.choice(range(0x80, 0xA0))
        elif p <= .80:     # replace initial byte by F0 and secobd byte 
            if s >= 0x80:  # by incompatible value, if multi-byte
                b[0] = 0xF0
                b[1] = random.choice(range(0x80, 0x90))
        return bytes(b)
    
    def decode(self, infile, outfile, errors):
        b = open(infile, 'rb').read()
        with open(outfile, 'w', newline='\n') as fout:
            for c in self.decoder(b, errors):
                fout.write('U+%04X\n' %c)
                
    def decoder(self, b, errors):
        if errors == 'none':
            return codepoints(b.decode())
        elif errors == 'ignore':
            return codepoints(b.decode(errors = 'ignore'))
        elif errors == 'replace':
            return self.fffdDecoder(b)
        
    def fffdDecoder(self, s):
        replace = 0xFFFD
        idx = 0
        try:
            while idx < len(s):
                b1 = s[idx]
                idx += 1
                if b1 <= 0x7F:       # ASCII
                    yield b1
                    continue
                if b1 <= 0xC1 or b1 >= 0xF5:
                    yield replace
                    continue
                b2 = s[idx]
                if ( (b2 & 0xC0) != 0x80 or
                     b1 == 0xE0 and b2 < 0xA0 or
                     b1 == 0xED and b2 > 0x9F or
                     b1 == 0xF0 and b2 < 0x90 or
                     b1 == 0xF4 and b2 >0x8F):
                    yield replace
                    continue
                if b1 <= 0xDF:        # two-byte code
                    yield ord(s[idx-1:idx+1].decode())
                    idx += 1
                    continue
                idx += 1
                b3 = s[idx]
                if (b3 & 0xC0) != 0x80:
                    yield replace
                    continue
                if b1 <= 0xEF:         # three-byte code
                    yield ord(s[idx-2:idx+1].decode())
                    idx += 1
                    continue
                idx += 1
                b4 = s[idx]
                if (b4 & 0xC0) != 0x80:
                    yield replace
                else:                   # four-byte code
                    yield ord(s[idx-3:idx+1].decode())
                    idx += 1
        except IndexError:
            yield replace
            raise StopIteration
        except UnicodeDecodeError:
            print("idx = ",idx)
            
    def saveParms(self):
        parms = {}
        parms['codelen'] = self.codes.get()
        parms['sampleSize'] = self.sample.get()
        parms['dataDir'] = self.dir.get()
        parms['jar'] = self.mars.get()
        parms['program'] = self.prog.get()
        parms['basename'] = self.fileBase.get()
        parms['errs'] = self.errors.get()
        
        with open('decoder.ini', 'wb') as fout:
            pickle.dump(parms, fout)
            
    def rerun(self):
        self.test()
        
    def writeLog(self):
        logname = os.path.join(self.dir.get(), 'dlog.txt')
        with open(logname, 'a') as fout:
            fout.write(self.text.getText())
            
    def help(self):
        helpText = '''This app will generate test inputs for your decoder program, and compute the expected outputs.  It will run your MIPS program, and compare the computed output to the expected output.

For each test you run, you will need to supply a base name for the files created.  If the base name is "dtest1", say, then the test input file will be named dtest1.bin,the expected output file will be named dtest1.txt, and the output file computed by your MIPS program will be named dtest1_mips.txt.  If the computed output differs from the expected output, an html file displaying the differences will also be created.  In our example, it will be called dtest1.htm.  

Finally, a log file, named dlog.txt, will be created, containing all output to the console area of the app.  Output from each session is appended to the log.  If you don't want this, you can just delete the log, and a new one will be created.

All these files will be placed in whatever directory you designate as the data directory.

The "New Test" creates a new random input file.  The "Re-Run" button will rerun an old test, using an existing input file.  You just have to place the base name in the appropriate entry box.  '''
        
        win = Toplevel()
        win.title('Decoder Test Help')
        text = Text(win, wrap= WORD)
        text.insert(END, helpText)
        text.configure(state=DISABLED)
        ok = Button(win, text='Okay', command = win.destroy)
        text.pack()
        ok.pack()
        
def wrapup():
    app.saveParms()
    app.writeLog()
    root.destroy()
    root.quit()

if __name__ == '__main__':
    random.seed()
    try:
        parms = pickle.load(open('decoder.ini', 'rb'))
    except (IOError, EOFError):
        parms = {}
    root = Tk()
    root.resizable(0,0)
    root.title('UTF-8 Decoding Test')
    app = DecoderApp(root, parms, bd=2, relief=GROOVE)
    app.grid()
    root.wm_protocol("WM_DELETE_WINDOW", wrapup)
    root.mainloop()

