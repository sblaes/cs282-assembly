		.data
error_hex:		.asciiz "Invalid input encountered in function getHexNum"
error_enc:		.asciiz	"Bad unicode value encountered in encode function"
		.globl encoder
	
# Function char* encoder(char *inBuffer, char* end, char* outBuffer)
# Registers
#	$a0	char *inBuffer, pointer to the input buffer
#	$a1	char *end, pointer to memory address at end of inBuffer
#	$a2	char *outBuffer, pointer to location in output buffer to write the value
#	$v0	char*, pointer to the output buffer
#	$s0	store cursor across function calls
#	$s1	store end of inBuffer pointer across function calls
#	$s2	store outBuffer pointer across function calls
#	$s3	linebuffer
#	$t0	temp for hex num to encode
#
# Algnorithm:
# 
# char* encoder(char *inBuffer char* end, char* outBuffer)
#     while inBuffer < end
#         inBuffer = getLine(inBuffer, lineBuffer)
#         rstrip(lineBuffer)
#         value = getHexNum(lineBuffer)
#         outBuffer = encode(value, outBuffer)
# return outBuffer

		.text
encoder:	# prologue
		addiu 	$sp, $sp, -32			# allocate stack mem
		sw 	$ra, ($sp)			# store ra on the stack
		sw 	$s0, 4($sp)			# store s0 on the stack	
		sw 	$s1, 8($sp)			# store s1 on the stack	
		sw 	$s2, 12($sp)			# store s2 on the stack	
		sw 	$s3, 16($sp)			# store s3 on the stack	
		
		move 	$s0, $a0			# cursor = inBuffer
		move 	$s1, $a1			# store end of string in s reg
		move 	$s2, $a2			# store output buffer pointer in s reg
		addi	$s3, $sp, 20			# store address of linebuffer (char[10])
encoder_loop:
		beq 	$s1, $s0, encoder_done  	# while inBuffer != end
		
		move 	$a0, $s0			# inBuffer = cursor
		move 	$a1, $s3			# linebuffer = outBuffer
		jal 	getLine				# call getLine(inbuffer, linebuffer)
		move 	$s0, $v0			# cursor = getLine(inbuffer, linebuffer)

		move	$a0, $s3			# linebuffer = linebuffer
		jal 	rstrip				# call rstrip(linebuffer)
			
		move	$a0, $s3			# linebuffer = linebuffer
		jal 	getHexNum			# value = getHexNum(lineBuffer)
		move 	$t0, $v0			# copy value to temp
		
		move	$a0, $t0			# value = value
		move	$a1, $s2			# outBuffer
		jal	encode 				# call encode(value, outbuffer)
		move 	$s2, $v0			# outbuffer= encode(value, outbuffer)
		
		b	encoder_loop

		
encoder_done:	
		move 	$v0, $s2			# return char* - end of output buffer
		#epilogue
		lw 	$ra, ($sp)			# restore return address
		lw 	$s0, 4($sp)			# restore s0
		lw 	$s1, 8($sp)			# restore s1
		lw 	$s2, 12($sp)			# restore s2
		lw 	$s3, 16($sp)			# restore s3
		addiu 	$sp, $sp, 32			# deallocate the stack
		
		jr 	$ra
	
	
# Homework 1
# This function implements the getLine function and executes it against a
# hard-coded string (see value of inBuffer below).  The getLine function
# locates the end of the line by searching for line feed character and
# copies all values into the line buffer such that the line of text
# may be printed.

# Function char* getLine(char *inPBuffer, char *lineBuffer)
# Registers
#	$a0	char *inBuffer, cursor1
#	$a1	char *lineBuffer, cursor2
#	$v0	RETURN char*, updated pointer to inBuffer
#	$t0	c, current character
#
# Algnorithm:
# cursor1 = inBuffer
# cursor2 = lineBuffer
# repeat:
#	c = *cursor1
#	*cursor2 = c
#	cursor1 += 1
#	cursor2 += 1
# until c == '\n'
# return cursor1

getLine:
		lb	$t0, ($a0)			# c = *cursor1
		sb	$t0, ($a1)			# *cursor2 = c
		addi	$a0, $a0, 1			# cursor1 += 1
		addi	$a1, $a1, 1			# cursor2 += 1
		beq	$t0, '\n', getLine_Done 	# exit if char is end line
		j	getLine				# otherwise, loop back around

getLine_Done:						# exit the getLine loop
		sb	$zero, ($a1)			# add terminating null
		move	$v0, $a0			# return cursor1
		jr	$ra				# go back to where we came from

# Function int getHexNum(char* inBuffer)
# Registers
#	$a0	char* inBuffer, cursor to the codepoint to be encoded
#	$a1	char *outBuffer, pointer to location in output buffer to write the value
#	$v0	char*, pointer to the current end of output buffer after it's advanced
#	$t0	running total
#	$t1	temp storage for shift operations
#
# Algnorithm:
# 
# if *inBuffer != 'U':
#     exit('Expected U�)
# inBuffer += 1
# if *inBuffer != '+':
#     exit(�expected +�)
#
# total = 0
# inBuffer += 1
# while *inBuffer != 0:
#     total <<= 4 // multiply by 16
#     if '0' <= *inBuffer <= '9':
#         total += *inBuffer - '0'
#     else if 'A' <= *inBuffer <= 'F':
#         total += *inBuffer - 'A' + 10
#     else: exit('Illegal digit')
#         inBuffer += 1
#
# return total

getHexNum:
		lbu 	$t0, ($a0)			# t0 = *cursor
		bne	$t0, 'U', getHexNum_invalid	# t0 != 'U'
		addi 	$a0, $a0, 1			# cursor += 1
		lbu 	$t0, ($a0)			# t0 = *cursor
		bne	$t0, '+', getHexNum_invalid	# t0 != '+'
		li	$t1, 0 				# total = 0
		
getHexNum_loop:
		addi 	$a0, $a0, 1			# cursor += 1
		lbu	$t0, ($a0)			# t0 = *cursor
		
		beqz	$t0, getHexNum_done		# while t0 != 0
		
		sll	$t1, $t1, 4			# total <<= 4
		
		blt	$t0, '0', getHexNum_invalid	# invalid if code < '0'
		ble	$t0, '9', getHexNum_num		# if '0' <= t0 <= '9', go to num
		
		blt	$t0, 'A', getHexNum_invalid 	# invalid if code < 'A'
		ble	$t0, 'F', getHexNum_let 	# if 'A' <= t0 <= 'F', go to let
		
		b	getHexNum_invalid		# invalid character if we get here
		
getHexNum_num:						# numeric hex value 0-9
		add 	$t1, $t1, $t0			# total += *inBuffer
		subi  	$t1, $t1, '0'			# total -= '0'
		b 	getHexNum_loop

getHexNum_let:						# alpha hex value A-F
		add 	$t1, $t1, $t0			# total += *inBuffer
		subi 	$t1, $t1, 'A'			# total -= 'A'
		addi 	$t1, $t1, 10			# total += 10
		b 	getHexNum_loop
		
getHexNum_done:
		move 	$v0, $t1			# return total
		jr 	$ra				# return


getHexNum_invalid:
		la	$a0, error_hex
		la	$a1, error_hex
		j	fatal

# rstrip implementation
# This program implements the rstrip function against a hard-coded string
		
# Function char* rstrip(char *buffer)
# Registers
#	$a0	char *buffer, cursor
#	$t0	c, current character
#
# Algnorithm:
# cursor = buffer
# while *cursor != '\0':
#	cursor += 1
# cursor -= 1
# while *cursor  = '\n' or '\r' or '\t' or ' ':
#	*cursor = '\0'
#	cursor -= 1

rstrip:
		lb	$t0, ($a0)			# c = *cursor
		beqz	$t0, rstrip_SetNullLoop		# exit if *cursor == '\0' 
		addi	$a0, $a0, 1			# cursor += 1
		j	rstrip				# loop back
		
rstrip_SetNullLoop:
		sb	$zero, ($a0)			# *cursor = '\0'
		addi	$a0, $a0, -1			# cursor -= 1
		lb	$t0, ($a0)			# c = *cursor
		beq	$t0, '\n', rstrip_SetNullLoop	# loop back around
		beq	$t0, '\r', rstrip_SetNullLoop	# loop back around
		beq	$t0, '\t', rstrip_SetNullLoop	# loop back around
		beq	$t0, ' ', rstrip_SetNullLoop	# loop back around
		jr	$ra				# go back to where we came from

# encoder implementation
# This program implements the UTF-8 encoder routine, converting a hex integer
# input into a UTF-8 formatted binary output.

# Function char* encode(int value, char *outBuffer).
# Registers
#	$a0	int value, the hex unicode codepoint to be encoded
#	$a1	char *outBuffer, pointer to location in output buffer to write the value
#	$v0	char*, pointer to the current end of output buffer after it's advanced
#	$t0	temporary variable used for calculations and shift operations
#	$t1	temporary variable used for calculations and shift operations
#
# Algnorithm:
# if (value > 0x10FFFF)
#	print error & return
# else if (value > 0xFFFF)
#	set val = 11110000 10000000 10000000 10000000
#	shift left 6 bits, copy top 3 bits of character into val
#	shift left 4 bits, copy 2nd word 6 bits of character into val
#	shift left 2 bits, copy 3rd word 6 bits of character into val
#	copy 4th word 6 bits of character into val
# else if (value > 0x7FF)
#	set val = 11100000 10000000 10000000
#	shift left 4 bits, copy top 4 bits of character into val
#	shift left 2 bits, copy 3rd word 6 bits of character into val
#	copy 3rd word 6 bits of character into val
# else if (value > 0x7f)
#	set val = 11000000 10000000
#	shift left 2 bits, copy top 5 bits of character into val
#	copy 2nd word 6 bits of character into val
# else
#	set val = 00000000
#	copy full word of character into val
encode:
		bgtu	$a0, 0x10FFFF, encode_error	# jump to error if invalid character
		bgtu	$a0, 0xFFFF, encode_4b		# 4 byte character
		bgtu	$a0, 0x7FF, encode_3b		# 3 byte character
		bgtu	$a0, 0x7f, encode_2b		# 2 byte character
		b	encode_1b			# 1 byte character
		
encode_4b:	# 4 byte character
		li	$t0, 0xF0808080			# init with 11110000 10000000 10000000 10000000

		sll	$t1, $a0, 6			# shift left 6 bits
		andi	$t1, $t1, 0x07000000		# clear all bits other than 1st word least 3 bits
		or	$t0, $t0, $t1			# copy remaining bits to temp register

		sll	$t1, $a0, 4			# shift left 4 bits
		andi	$t1, $t1, 0x003F0000		# clear all bits other than 2nd word least 6 bits
		or	$t0, $t0, $t1			# copy remaining bits to temp register

		sll	$t1, $a0, 2			# shift left 2 bits
		andi	$t1, $t1, 0x00003F00		# clear all bits other than 3rd word least 6 bits
		or	$t0, $t0, $t1			# copy remaining bits to temp register

		andi	$t1, $a0, 0x0000003F		# clear all bits other than 4th word least 6 bits
		or	$t0, $t0, $t1			# copy remaining bits to temp register
		
		sb	$t0 3($a1)			# store 1st byte in output buffer
		srl	$t0, $t0, 8			# shift 8 bits to store next byte
		sb	$t0 2($a1)			# store 2nd byte in output buffer
		srl	$t0, $t0, 8			# shift 8 bits to store next byte
		sb	$t0 1($a1)			# store 3rd byte in output buffer
		srl	$t0, $t0, 8			# shift 8 bits to store next byte
		sb	$t0 0($a1)			# store 4th byte in output buffer
		
		addi	$v0, $a1, 4			# increment the output buffer pointer and return it
		b	encode_return

encode_3b:	# 3 byte character
		li	$t0, 0x00E08080			# init with 11100000 10000000 10000000

		sll	$t1, $a0, 4			# shift left 4 bits
		andi	$t1, $t1, 0x000F0000		# clear all bits other than 1st word least 3 bits
		or	$t0, $t0, $t1			# copy remaining bits to temp register

		sll	$t1, $a0, 2			# shift left 2 bits
		andi	$t1, $t1, 0x00003F00		# clear all bits other than 2nd word least 6 bits
		or	$t0, $t0, $t1			# copy remaining bits to temp register

		andi	$t1, $a0, 0x0000003F		# clear all bits other than 3rd word least 6 bits
		or	$t0, $t0, $t1			# copy remaining bits to temp register
		
		sb	$t0 2($a1)			# store 1st byte in output buffer
		srl	$t0, $t0, 8			# shift 8 bits to store next byte
		sb	$t0 1($a1)			# store 2nd byte in output buffer
		srl	$t0, $t0, 8			# shift 8 bits to store next byte
		sb	$t0 0($a1)			# store 3rd byte in output buffer
		
		addi	$v0, $a1, 3			# increment the output buffer pointer and return it
		b	encode_return

encode_2b:	# 2 byte character
		li	$t0, 0x0000C080			# init with 11000000 10000000

		sll	$t1, $a0, 2			# shift left 2 bits
		andi	$t1, $t1, 0x00003F00		# clear all bits other than 1st word least 3 bits
		or	$t0, $t0, $t1			# copy remaining bits to temp register

		andi	$t1, $a0, 0x0000003F		# clear all bits other than 2nd word least 6 bits
		or	$t0, $t0, $t1			# copy remaining bits to temp register
		
		sb	$t0 1($a1)			# store 1st byte in output buffer
		srl	$t0, $t0, 8			# shift 8 bits to store next byte
		sb	$t0 0($a1)			# store 2nd byte in output buffer
		
		addi	$v0, $a1, 2			# increment the output buffer pointer and return it
		b	encode_return

encode_1b:	# 1 byte character
		sb	$a0, ($a1)			# store 1st byte in output buffer
		
		addi	$v0, $a1, 1			# increment the output buffer pointer and return it
		b	encode_return

encode_return:
		jr	$ra				# return back to caller
		
encode_error:
		la	$a0, error_enc
		la	$a1, error_enc
		j	fatal
