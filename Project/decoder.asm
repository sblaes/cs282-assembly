# Title: Decoder Function (with error handling)

# Algorithm:
# char* decoder(char* inBuff, char* end, char* outBuff, char errors)
#	cursor = inBuff
#	while cursor < end:
#		b2 = b3 = b4 = 0
#		b1 = *cursor
#		cursor += 1
#		if b1 <= 0x7F:	// ASCII
#			<output b1>
#			continue
#		if b1 ? 0XC1 or b1 ? 0xF5: 
#			// if b1 < 0xC0 we have a continuation byte, 
#			// otherwise we have illegal 0xC0 or 0xC1 or 0xF8 � 0xFF 
#			<replace and continue>
#		// we have a multi-byte code, so get the second byte
#		if cursor == end:
#			<replace and continue>
#		b2 = *cursor
#		if (b2 & 0xC0) ? 0x80: // not a continuation byte
#			<replace and continue>
#		// check for ill-formed sequences
#		if (b1 == 0xE0 and b2 < 0xA0) or
#		   (b1 == 0xED and b2 > 0x9F) or
#		   (b1 == 0xF0 and b2 < 0x90) or
#		   (b1 == 0xF4 and b2 > 0x8F)
#			<replace and continue>
#		if b1 <= 0xDF:	// two-byte code
#			<decode and advance>
#		// get the third byte
#		cursor += 1
#		if cursor == end:
#			<replace and continue>
#		b3 = *cursor
#		if (b3 & 0xC0) ? 0x80: // not a continuation byte
#			<replace and continue>
#		if b1 <= 0xEF:	// three-byte code
#			<decode and advance>
#		// get the fourth byte
#		cursor += 1
#		if cursor == end:
#			<replace and continue>
#		b4 = *cursor
#		if (b4 & 0xC0) ? 0x80: // not a continuation byte
#			<replace and continue>
#		<decode and advance>
#	return outBuff

# <decode and advance> means, call decode(b1, b2, b3, b4), output the value return,
#	increment the cursor, and jump to the top of the loop.

# <replace and continue> means, output the replacement character and jump to the 
#	top of the loop.

# Registers:
# s0: char* inBuff
# s1: char* end
# s2: char* outBuff
# s3: char  errors

		.globl decoder
		.text
decoder:
		addi $sp, $sp, -20		# allocate stack memory
		sw $ra, ($sp)			# store the ra
		sw $s0, 4($sp)			# store s0
		sw $s1, 8($sp)			# store s1
		sw $s2, 12($sp)			# store s2
		sb $s3, 16($sp)			# store s3
		
		move $s0, $a0			# move the inBuff to s0
		move $s1, $a1			# move end to s1
		move $s2, $a2			# move the outBuff to s2
		move $s3, $a3			# move the error indicator to s3
		
decoder_loop:
		beq $s1, $s0, decoder_done	# while cursor < end:
		
		move $a3, $zero			# b4 = 0
		move $a2, $zero			# b3 = 0
		move $a1, $zero			# b2 = 0
		lbu $a0, ($s0)			# b1 = *cursor
		addiu $s0, $s0, 1		# cursor += 1
		
		bgt $a0, 0x7F, decoder_code2 	# if b1 <= 0x7F:
		move $a1, $s2			# output b1
		jal hexWrite
		move $s2, $v0
		b decoder_loop			# continue
		
decoder_code2:
		blt $a0, 0xC2, decoder_rac	# if b1 <= 0XC1 or 
		bgt $a0, 0xF4, decoder_rac	#    b1 >= 0xF5: replace & cont
		
		beq $s1, $s0, decoder_rac	# if cursor == end: replace & cont

		lbu $a1, ($s0)			# b2 = *cursor
		
		andi $t0, $a1, 0xC0		# t0 = b2 & 0xC0
		bne $t0, 0x80, decoder_rac	# if (b2 & 0xC0) ? 0x80: replace & cont, not a continuation byte
		
		bne $a0, 0xE0, decoder_code2_if2 # if (b1 == 0xE0 and
		blt $a1, 0xA0, decoder_rac	#     b2 < 0xA0): replace & cont
decoder_code2_if2:
		bne $a0, 0xED, decoder_code2_if3 # if (b1 == 0xED and
		bgt $a1, 0x9F, decoder_rac	#     b2 > 0x9F): replace & cont
decoder_code2_if3:
		bne $a0, 0xF0, decoder_code2_if4 # if (b1 == 0xF0 and
		blt $a1, 0x90, decoder_rac	#     b2 < 0x90): replace & cont
decoder_code2_if4:
		bne $a0, 0xF4, decoder_code2_endif # if (b1 == 0xF4 and
		bgt $a1, 0x8F, decoder_rac	#     b2 > 0x8F): replace & cont

decoder_code2_endif:
		bgt $a0, 0xDF, decoder_code3 	# if b1 <= 0xDF:
		b decoder_decode_and_advance	# <decode and advance>
		
decoder_code3:
		addiu $s0, $s0, 1		# cursor += 1
		beq $s1, $s0, decoder_rac	# if cursor == end: replace & cont
		
		lbu $a2, ($s0)			# b3 = *cursor

		andi $t0, $a2, 0xC0		# t0 = b3 & 0xC0
		bne $t0, 0x80, decoder_rac	# if (b3 & 0xC0) ? 0x80: replace & cont, not a continuation byte
				
		bgt $a0, 0xEF, decoder_code4 	# if b1 <= 0xEF
		b decoder_decode_and_advance	# <decode and advance>
		
decoder_code4:
		addiu $s0, $s0, 1		# cursor += 1
		beq $s1, $s0, decoder_rac	# if cursor == end: replace & cont

		lbu $a3, ($s0)			# b4 = *cursor

		andi $t0, $a3, 0xC0		# t0 = b4 & 0xC0
		bne $t0, 0x80, decoder_rac	# if (b4 & 0xC0) ? 0x80: replace & cont, not a continuation byte
		
decoder_decode_and_advance:
		jal decode			# call decode(b1, b2, b3, b4)
		move $a0, $v0
		move $a1, $s2
		jal hexWrite			# call hexWrite(val, buf)
		move $s2, $v0
		addiu $s0, $s0, 1		# cursor += 1
		b decoder_loop			# continue
		
decoder_rac:
		beq $s3, '1', decoder_loop	# continue
		li $a0, 0xFFFD			# print replacement char
		move $a1, $s2
		jal hexWrite			# call hexWrite(val, buf)
		move $s2, $v0
		b decoder_loop
		
decoder_done:					# epilog
		move $v0, $s2
		
		lw $ra, ($sp)			# reclaim ra
		lw $s0, 4($sp)			# reclaim s0
		lw $s1, 8($sp)			# reclaim s1
		lw $s2, 12($sp)			# reclaim s2
		lb $s2, 16($sp)			# reclaim s3
		
		addi $sp, $sp, 20		# deallocate stack
		
		jr $ra

# Author: Max Coker
# Fucntion Title: Decode
# Date: 3-18-2012

# Algorithm:
# int decode(int b1, int b2, int b3, int b4)
#	if b4 ? 0: // 4-byte code
#		b4 = b4 & 0x3F
#		b3 = (b3 & 0x3F) << 6
#		b2 = (b2 & 0x3F) << 12
#		b1 = (b1 & 0x07) << 18
#	else if b3 ? 0: // 3-byte code
#		b3 = b3 & 0x3F
#		b2 = (b2 & 0x3F) << 6
#		b1 = (b1 & 0x0F) << 12
#	else if b2 ? 0: // 2-byte code
#		b2 = b2 & 0x3F
#		b1 = (b1 & 0x1F) << 6
#	return b1 + b2 + b3 + b4

# Registers:
# a0: int b1
# a1: int b2
# a2: int b3
# a3: int b4

decode:
		bnez $a3, decode_4b	# 4 byte value
		bnez $a2, decode_3b	# 3 byte value
		bnez $a1, decode_2b	# 2 byte value
		b decode_return		# 1 byte value
		
decode_4b:
		andi $a3, $a3, 0x3F	# trim all but lower 6 bits
		
		andi $a2, $a2, 0x3F	# trim all but lower 6 bits
		sll $a2, $a2, 6		# shift left to store in third byte
		
		andi $a1, $a1, 0x3F	# trim all but lower 6 bits
		sll $a1, $a1, 12	# shift left to store in 2nd byte
		
		andi $a0, $a0, 0x07	# trim all but lower 3 bits
		sll $a0, $a0, 18	# shift left to store in 1st byte
		
		b decode_return
		
decode_3b:
		andi $a2, $a2, 0x3F	# trim all but lower 6 bits
		
		andi $a1, $a1, 0x3F	# trim all but lower 6 bits
		sll $a1, $a1, 6		# shift left to store in 2nd byte
		
		andi $a0, $a0, 0x0F	# trim all but lower 4 bits
		sll $a0, $a0, 12	# shift left to store in 1st byte
		
		b decode_return
		
decode_2b:
		andi $a1, $a1, 0x3F	# trim all but lower 6 bits
		
		andi $a0, $a0, 0x1F	# trim all but lower 5 bits
		sll $a0, $a0, 6		# shift left to store in 1st byte
		
		b decode_return
		
decode_return:
		add $v0, $a0, $a1	# concatenate all values together to one int and return
		add $v0, $v0, $a2
		add $v0, $v0, $a3
		
		jr $ra

# Function char* hexWrite(int value, char* outBuffer)
# Registers
#	$a0	int value, the 32 bit integer value to be converted to hex
#	$a1	char *outBuffer, pointer to location in output buffer to write the value
#	$v0	char*, pointer to the current end of output buffer after it's advanced
#	$t0	temporary counter variable and temp char storage
#	$t1	temp variable for arithmetic
#
# Algnorithm:
# 
# int chars = 4
# if (value > 0x000FFFFF)
#   chars = 6
# else if (value > 0x0000FFFF)
#   chars = 5
# 
# for (int i = 4*(chars-1); i >= 0; i -= 4) {
#   char temp = value >> i;
#   temp = temp & 0x0F
#   if (temp > 9)
#     temp = temp - 10 + 'A'
#   else
#     temp = temp + '0'
#   *outBuffer = temp
#   outBuffer += 1

hexWrite:
		li	$t0, 0x55			# store U+ prefix in buffer
		sb	$t0, 0($a1)
		li	$t0, 0x2B
		sb	$t0, 1($a1)
		addi	$a1, $a1, 2
		li	$t0, 4				# int chars = 4
		ble	$a0, 0xFFFF, hexWrite_endif1	# if value > 0xFFFF
		li	$t0, 5				#   chars = 5
		ble	$a0, 0xFFFFF, hexWrite_endif1	# if value > 0xFFFFF
		li	$t0, 6				#   chars = 6

hexWrite_endif1:
		subi	$t0, $t0, 1			# i = 4*(chars-1)
		sll	$t0, $t0, 2			
		
hexWrite_loop:
		srlv	$t1, $a0, $t0			# char temp = value >> i;
		andi	$t1, $t1, 0x0F			# temp = temp & 0x0F
		blt	$t1, 0x0A, hexWrite_else	# if (temp > 9)
		addi	$t1, $t1, 0x37			# temp = temp - 10 + 'A'
		b	hexWrite_endif2			# else

hexWrite_else:
		addi	$t1, $t1, '0'			# temp = temp + '0'

hexWrite_endif2:
		sb	$t1, ($a1)			# *outBuffer = temp
		addi	$a1, $a1, 1			# outBuffer += 1
		
		beqz	$t0, hexWrite_endfor		# if i >= 0, continue loop
		subi	$t0, $t0, 4			# i -= 4
		j hexWrite_loop

hexWrite_endfor:
		li	$t0, '\n'
		sb	$t0, ($a1)			# outBuffer = "\n"
		addi	$a1, $a1, 1
		move	$v0, $a1
		jr	$ra
