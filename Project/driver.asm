# Max Coker
# Project 1 Driver
# Date Modified: 3-12-2012
# This will call all of the functions
# It will also read the input file into the input Buffer
# and write the output buffer to the output file.

# Registers
# $a1: agument *cursor
# $s0: fdIn (file descriptor - In)
# $s1: fdout (file descriptor - Out)
# $s2: readBytes (after reading open file)
# $s3: *endOutputBuffer
# $s4: outBuffer_address
# $s6: Processing Code letter

		.data
IOError:	.asciiz	"An I/O error was encountered in the Driver function"
error_args:	.asciiz	"Invalid arguments were provided, arguments required are <processing code> <input file> <output file>"
error_proCode:	.asciiz	"Invalid processing code supplied, use 'e' to encode, or 'n' to decode"
errorHeader:	.asciiz "\n##################  ERROR  ##################\n"
newline:	.asciiz	"\n"
error_char:	.asciiz "U+FFFD"

		.globl main
		.globl fatal
		.text

main:
		addiu 	$sp, $sp, -131072		# allocate and assign in/out buffers on the stack

		bne 	$a0, 3, driver_badArgs		# if (argc != 3) goto Not3
		
		lw 	$t1, ($a1)			# t1 <-- *cursor
		lb	$s6, ($t1)			# First letter of processing code --> $s6
		addiu	$a1, $a1, 4			# increment the cursor to the next pointer
		
		beq	$s6, 'e', driver_goodCode	# if processing code == e
		beq	$s6, 'r', driver_goodCode	# if processing code == r
		bne 	$s6, 'i', driver_badCode	# or processing_code == i, else error

driver_goodCode:
		jal 	getFileNames			# goto function getFileNames($a0, $a1)
		move 	$s0, $v0			# first return value --> fdIn
		move 	$s1, $v1			# second return value --> fdOut
		
		li 	$v0, 14				# Read from File syscall
		move 	$a0, $s0			# assign fdIn
		move 	$a1, $sp			# load the address of the input buffer
		li 	$a2, 65536			# max Characters to read
		syscall
		
		blez 	$v0, driver_error			# check for syscall error
		
		move 	$s2, $v0			# Syscall return --> readBytes
		
		li 	$v0, 16				# close file syscall
		move 	$a0, $s0			# assign fdIn
		syscall
		
		move 	$a0, $sp			# a0 <-- char* inBuffer
		add 	$a1, $sp, $s2			# a1 <-- char* end
		addi 	$a2, $sp, 65536			# a2 <-- char* outBuffer
		
		beq	$s6, 'r'  driver_eDecoder	# if processing command == 'r' goto 'eDecoder'
		beq 	$s6, 'i', driver_decoder	# if processing command == 'n' goto 'decoder'
		
		jal 	encoder				# goto function 'encoder'
		move 	$s3, $v0			# first return value --> *endOutputBuffer
		
		j 	driver_epilog			# cleanup and return
		
driver_eDecoder:
		li	$a3, '2'			# load char errors = '2'
		jal	decoder				# goto fuction 'eDecoder'
		move	$s3, $v0			# first return value --> *endOutputBuffer
		j	driver_epilog			# cleanup and return

driver_decoder:
		li	$a3, '1'			# load char errors = '1'	
		jal 	decoder				# goto function 'decoder'
		move 	$s3, $v0			# first return value --> *endOutputBuffer
		
driver_epilog:		
		li 	$v0, 15				# write to file syscall
		move 	$a0, $s1			# assign fOut
		addi 	$a1, $sp, 65536			# load address of the Output Buffer
		sub	$a2, $s3, $a1			# assign endOutputBuffer
		syscall
		
		blez 	$v0, driver_error			# check for syscall error
		
		li 	$v0, 16				# close file syscall
		move 	$a0, $s1			# assign fdOut
		syscall
		
		addiu 	$sp, $sp, 131072		# deallocate the stack
		
		li 	$v0, 10				# graceful termination
		syscall

driver_badArgs:						# invalid parameters supplied, call error function
		la	$a0, error_args
		la	$a1, error_args
		j	fatal
		
driver_badCode:						# error encountered, call error function
		la	$a0, error_proCode
		la	$a1, error_proCode
		j	fatal
		
## remember to include I/O error checking
driver_error:		
		la	$a0, IOError
		la	$a1, IOError
		j	fatal

# This function is intended to render fatal errors
#
# Registers
# $a0: null terminated string to print to stdout
# $a1: null terminated string to show in message box
# $t0: temp storage for sysout error message
# $t1: temp storage for message box error message


fatal:		
		move	$t0, $a0			# store error message temporarily
		move	$t1, $a1			# store message box message temporarily
		
		li	$v0, 4				# print sysout error message header
		la	$a0, errorHeader
		syscall

		li	$v0, 4				# print sysout error message
		move	$a0, $t0
		syscall

		li	$v0, 4				# print new line
		la	$a0, newline
		syscall
		
		li	$v0, 55				# show message box error message
		move	$a0, $t1
		li	$a1, 0
		syscall
		
		li	$v0, 10				# graceful exit
		syscall
