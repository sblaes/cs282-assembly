# Max Coker
# Date Modified: 3-18-2012
# Function (int fdIn, int fdOut) getFileNames(int argc, char *argv[])
#
# this function will get the file names and 
# return the File descriptors of the open files.

# registers
# $a0: argc - number of parameters provided, also temp storage
# $a1: cursor - array of null terminated strings for each parameter
# $s0: fdIN  - input file descriptor
# $s1: fdOut - output file descriptor
		.data
openErr:	.asciiz	"Could not open file in function getOpenFile"

		.globl 	getFileNames
		
		.text

getFileNames:	
		addiu 	$sp, $sp, -12			# allocate stack mem
		sw 	$ra, ($sp)			# store ra on the stack
		sw 	$s0, 4($sp)			# store s0 on the stack
		sw 	$s1, 8($sp)			# store s1 on the stack

		lw 	$a0, ($a1)			# a0 <-- cursor = pointer to input filename
		move	$s0, $a0			# move cursor to saved register to persist across func call
		
		addiu 	$a1, $a1, 4			# advance the cursor 1 word
		
		lw 	$a0, ($a1)			# a0 <-- cursor = pointer to output filename
		
		li 	$a1, 1				# int flags = write-only with create
		move 	$a2, $zero			# mode ignored
		jal 	getOpenFile
		
		move 	$s1, $v0			# t1 <-- fdOut, save across func call

		move 	$a0, $s0			# *filename = fIn
		li 	$a1, 0				# int mode = read-only
		move 	$a2, $zero			# mode ignored		
		jal 	getOpenFile			# after syscall v0 <-- fdIn
		
		move 	$v1, $s1			# v1 <-- fdOut
		
		b	getFileNames_done
		
getFileNames_done:	
		lw 	$ra, ($sp)			# restore return address
		lw 	$s0, 4($sp)			# restore s0
		lw 	$s1, 8($sp)			# restore s1
		addiu 	$sp, $sp, 12			# deallocate the stack
		jr 	$ra				#return to main

# int getopenFile(char *Filename, int mode)
# registers
# $a0: char *filename
# $a1: int mode, 0=read, 1=write
# $v0: 

getOpenFile:	
		li 	$v0, 13	
		li	$a2, 0			# open file syscall, mode ignored
		syscall
		
		bltz 	$v0, getOpenFile_error	#if error opening file
		jr 	$ra

getOpenFile_error:		
		la	$a0, openErr
		la	$a1, openErr
		j	fatal
