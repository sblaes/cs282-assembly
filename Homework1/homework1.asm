# Author: Sean Blaes
# Modifed: 12 February 2012

# Homework 1
# This program implements the getLine function and executes it against a
# hard-coded string (see value of inBuffer below).  The getLine function
# locates the end of the line by searching for line feed character and
# copies all values into the line buffer such that the line of text
# may be printed.

		.data 
inBuffer:	.ascii	"This is line 1\r\nThis is line 2\nThis is line 3\n"
lineBuffer:	.space	65536

		.globl	main
		.text 
		
# Registers
#	$s0	cursor

main:
		la	$s0, inBuffer		# cursor = inBuffer

		move	$a0, $s0		# $a0 = cursor
		la	$a1, lineBuffer		# $a1 = lineBuffer
		jal	getLine			# call getLine(cursor, lineBuffer)
		move	$s0, $v0		# cursor = getLine return value
		
		li	$v0, 4			# print syscall
		la	$a0, lineBuffer		# print linebuffer
		syscall				# execute

		move	$a0, $s0		# $a0 = cursor
		la	$a1, lineBuffer		# $a1 = lineBuffer
		jal	getLine			# call getLine(cursor, lineBuffer)
		move	$s0, $v0		# cursor = getLine return value
		
		li	$v0, 4			# print syscall
		la	$a0, lineBuffer		# print linebuffer
		syscall				# execute

		move	$a0, $s0		# $a0 = cursor
		la	$a1, lineBuffer		# $a1 = lineBuffer
		jal	getLine			# call getLine(cursor, lineBuffer)
		move	$s0, $v0		# cursor = getLine return value
		
		li	$v0, 4			# print syscall
		la	$a0, lineBuffer		# print linebuffer
		syscall				# execute

		li		$v0, 10		# graceful termination
		syscall
		
# Function char* getLine(char *inPBuffer, char *lineBuffer)
# Registers
#	$a0	char *inBuffer, cursor1
#	$a1	char *lineBuffer, cursor2
#	$v0	RETURN char*, updated pointer to inBuffer
#	$t0	c, current character
#
# Algnorithm:
# cursor1 = inBuffer
# cursor2 = lineBuffer
# repeat:
#	c = *cursor1
#	*cursor2 = c
#	cursor1 += 1
#	cursor2 += 1
# until c == '\n'
# return cursor1
getLine:
		lb	$t0, ($a0)		# c = *cursor1
		sb	$t0, ($a1)		# *cursor2 = c
		addi	$a0, $a0, 1		# cursor1 += 1
		addi	$a1, $a1, 1		# cursor2 += 1
		beq	$t0, '\n', getLine_Done # exit if char is end line
		j	getLine			# otherwise, loop back around

getLine_Done:					# exit the getLine loop
		sb	$zero, ($a1)		# add terminating null
		move	$v0, $a0		# return cursor1
		jr	$ra			# go back to where we came from
