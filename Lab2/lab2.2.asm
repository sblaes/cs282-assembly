# Author: Sean Blaes
# Modifed: 1 February 2012, 3:48 PM

# Lab 2 Question 2
# Write a function named alphaNum that takes one character as an input 
# parameter and return 1 if that character is a letter, 2 if the character  
# is a digit, and 0 for anything else. Write a program that prompts the user  
# to enter a string of up to 80 characters, and then print out the number  
# of characters, letters, and digits in the string.

# Algorithm :

# cursor = buffer, countChars = 0, countLetters = 0, countDigits = 0
# c = *buffer 
# while *c != '\0':
#	if 'A' <= *c <= 'Z' or 'a' <= *c <= 'z':
#		countLetters += 1
#	else if '0' <= *c <= '9':
#		countDigits += 1
#	countChars += 1
#     ++cursor

		.data
greet:		.asciiz "Please enter a string: "
answer1:	.asciiz	"Your string has "
answer2:	.asciiz	" characters, "
answer3:	.asciiz	" letters, and "
answer4:	.asciiz	" digits.\n"
buffer:		.space	80

		.globl	main
		.text
		
#	Register usage:
#		$s0: cursor
#		$s1: countChars
#		$s2: *cursor
#		$s3: countLetters
#		$s4: countDigits

main:	
		li	  	$v0, 4		# print string
		la		$a0, greet	# string to print
		syscall
		
		li		$v0, 8		# read string
		la		$a0, buffer	# input buffer
		li		$a1, 80		# buffer length
		syscall
		
		li		$s1, 0		# countChars = 0
		li		$s3, 0		# countLetters = 0
		li		$s4, 0		# countDigits = 0
		la		$s0, buffer	# cursor = buffer
		lbu		$s2, ($s0)	# t2 <-- *cursor
		
loop:		beqz		$s2, done	# while *cursor != 0
		move		$a0, $s2	# a0 <-- s2
		jal		alphaNum	# call alphaNum
		beq		$v0, 1, incr_l
		beq		$v0, 2, incr_d
		b		next		# not a letter or digit if we get here
incr_l:		addi		$s3, $s3, 1	# countLetters += 1
		b next
incr_d:		addi		$s4, $s4, 1	# countDigits += 1
		b next
next:		addi		$s1, $s1, 1	# countChars += 1
		addiu		$s0, $s0, 1	# cursor++
		lbu		$s2, ($s0)	# t2 <-- *cursor
		b		loop			
		
done:		li	  	$v0, 4		# print string
		la		$a0, answer1	# string to print 
		syscall
		
		li		$v0, 1		# print int
		move		$a0, $s1	# print count
		syscall
		
		li	  	$v0, 4		# print string
		la		$a0, answer2	# string to print
		syscall
		
		li		$v0, 1		# print int
		move		$a0, $s3	# print count
		syscall
		
		li	  	$v0, 4		# print string
		la		$a0, answer3	# string to print
		syscall
		
		li		$v0, 1		# print int
		move		$a0, $s4	# print count
		syscall
		
		li	  	$v0, 4		# print string
		la		$a0, answer4	# string to print
		syscall
		
		li		$v0, 10		# graceful termination
		syscall
		
alphaNum: 					# alphaNum(a)
		blt		$a0, 'A', alphaNum_d	# not a letter if < 'A'
		ble		$a0, 'Z', alphaNum_l	# if <= 'Z' it's a letter
		blt		$a0, 'a', alphaNum_d	# not a letter if < 'a'
		ble		$a0, 'z', alphaNum_l	# if <= 'z' it's a letter

alphaNum_l: 					# alphaNum return 1, it's a letter
		li		$v0, 1
		jr		$ra		

alphaNum_d: 					# alphaNum check if it's a digit
		blt		$a0, '0', alphaNum_c	# not a digit if < '0'
		bgt		$a0, '9', alphaNum_c	# not a digit if > '9'
		li		$v0, 2
		jr		$ra		
		
alphaNum_c: 					# alphaNum return 0, not a letter or digit
		li		$v0, 0
		jr		$ra		
		
