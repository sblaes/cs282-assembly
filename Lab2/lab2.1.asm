# Author: Sean Blaes
# Modifed: 1 February 2012, 3:48 PM

# Lab 2 Question 1
# This is a small modification of question 2 from lab 1. You may use Saul�s 
# solution to that problem to get started.
# Rewrite the program to call a function named isLetter. The function should 
# take one input parameter (a character) and return 1 if that character is a 
# letter and 0 if it is not. To count the number of letters in the name, the 
# program should call isLetter for each character.

# Algorithm :

# cursor = buffer, count = 0
# c = *buffer 
# while *c != '\0':
#	if 'A' <= *c <= 'Z' or 'a' <= *c <= 'z':
#		count += 1
#     ++cursor
		.data
greet:		.asciiz "What is your name? "
answer1:	.asciiz	"Your name has "
answer2:	.asciiz	" letters.\n"
buffer:		.space	32

		.globl	main
		.text
		
#	Register usage:
#		$s0: cursor
#		$s1: count
#		$s2: *cursor

main:	
		li	  	$v0, 4		# print string
		la		$a0, greet	# string to print
		syscall
		
		li		$v0, 8		# read string
		la		$a0, buffer	# input buffer
		li		$a1, 32		# buffer length
		syscall
		
		li		$s1, 0		# count = 0
		la		$s0, buffer	# cursor = buffer
		lbu		$s2, ($s0)	# t2 <-- *cursor
		
loop:		beqz		$s2, done	# while *cursor != 0
		move		$a0, $s2	# a0 <-- s2, function arg
		jal		isLetter	# call isLetter
		beq		$v0, 1, incr
		b		next		# not a letter if we get here
incr:		addi		$s1, $s1, 1	# count += 1
next:		addiu		$s0, $s0, 1	# cursor++
		lbu		$s2, ($s0)	# t2 <-- *cursor
		b		loop			
		
done:		li	  	$v0, 4		# print string
		la		$a0, answer1	# string to print
		syscall
		
		li		$v0, 1		# print int
		move		$a0, $s1	# print count
		syscall
		
		li	  	$v0, 4		# print string
		la		$a0, answer2	# string to print
		syscall
		
		li		$v0, 10		# graceful termination
		syscall
		
isLetter: 					# isLetter(a)
		blt		$a0, 'A', isLetter_f	# not a letter if < 'A'
		ble		$a0, 'Z', isLetter_t	# if <= 'Z' it's a letter
		blt		$a0, 'a', isLetter_f	# not a letter if < 'a'
		ble		$a0, 'z', isLetter_t	# if <= 'z' it's a letter

isLetter_t: 					# isLetter return 1
		li		$v0, 1
		jr		$ra		

isLetter_f: 					# isLetter return 0
		li		$v0, 0
		jr		$ra		
		
		
