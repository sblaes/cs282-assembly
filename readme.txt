CS-282 Project Phase 1
Team #5

Max Coker
Casey Witkowski
Sean Blaes

driver.asm: The main function which orchestrates the other functions
encode.asm: Implements char* encode(int value, char *outBuffer), encodes a unicode text
    to a UTF-8 hex code point
encoder.asm: Implements char* encoder(char *inBuffer, char* end, char* outBuffer), iterates
    the inBuffer, encodes all values to UTF-8, and copies them to the output buffer
fatal.asm: Called when fatal errors occurs, prints an error message to the console
    and displays an error dialog.
getFileNames.asm: Implements (int fdIn, int fdOut) getFileNames(int argc, char *argv[]),
    retrieves file names passed in as program arguments, and opens the files associated 
    with them
getHexNum.asm: Implements int getHexNum(char* inBuffer), gets the next value from the input
    buffer and converts it to an integer
getline.asm: Implements char* getLine(char *inPBuffer, char *lineBuffer), retrieves the next
    line of input from the inbuffer
getOpenFile.asm: Implements getopenFile(char *Filename, int mode), helper function to open
    the requested file
rstrip.asm: Implements char* rstrip(char *buffer), removes whitespace from the right side of
    the string

