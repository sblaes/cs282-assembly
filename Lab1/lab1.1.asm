# Author: Saul Spatz
# Modifed: 29 January 2012, 4:445 pm

# Lab 1 Question 2
# Write a program that asks the user to enter his name, and counts the number of letters in his name.

# Algorithm :

# cursor = buffer, count = 0
# c = *buffer 
# while *c != '\0':
#	if 'A' <= *c <= 'Z' or 'a' <= *c <= 'z':
#		count += 1
#     ++cursor

		.data
greet:	.asciiz 	"What is your name? "
answer1:	.asciiz	"Your name has "
answer2:	.asciiz	" letters.\n"
buffer:	.space	32

		.globl	main
		.text
		
#	Register usage:
#		$t0: cursor
#		$t1: count
#		$t2: *cursor

main:	
		li	  	$v0, 4		# print string
		la		$a0, greet	# string to print
		syscall
		
		li		$v0, 8		# read string
		la		$a0, buffer	# input buffer
		li		$a1, 32		# buffer length
		syscall
		
		li		$t1, 0		# count = 0
		la		$t0, buffer	# cursor = buffer
		lbu		$t2, ($t0)		# t2 <-- *cursor
		
loop:		beqz		$t2, done		# while *cursor != 0
		blt		$t2, 'A', next	# not a letter if < 'A'
		ble		$t2, 'Z', incr	# if <= 'Z' it's a letter
		blt		$t2, 'a', next	# not a letter if < 'a'
		ble		$t2, 'z', incr	# if <= 'z' it's a letter
		b		next			# not a letter if we get here
incr:		addi		$t1, $t1, 1	# count += 1
next:	addiu	$t0, $t0, 1	# cursor++
		lbu		$t2, ($t0)		# t2 <-- *cursor
		b		loop			
		
done:	li	  	$v0, 4		# print string
		la		$a0, answer1	# string to print
		syscall
		
		li		$v0, 1		# print int
		move	$a0, $t1		# print count
		syscall
		
		li	  	$v0, 4		# print string
		la		$a0, answer2	# string to print
		syscall
		
		li		$v0, 10		# graceful termination
		syscall